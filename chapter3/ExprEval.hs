--import Pretty
import Data.Maybe
import Text.Parsec 
import Text.Parsec.String (Parser)
import qualified Text.Parsec.Token as Tok

import qualified Text.Parsec.Expr as Ex
  
import Data.Functor.Identity  

import System.Console.Haskeline
import Control.Monad.Trans


langDef :: Tok.LanguageDef ()
langDef = Tok.LanguageDef {
  Tok.commentStart = "{-"
  ,Tok.commentEnd = "-}"
  ,Tok.commentLine = "--"
  ,Tok.nestedComments = True
  ,Tok.identStart = letter
  ,Tok.identLetter = alphaNum <|> oneOf "-'"
  ,Tok.opStart = oneOf ":!#$%&*+./<=>?@\\^|-~"
  ,Tok.opLetter = oneOf ":!#$%&*+./<=>?@\\^|-~"
  ,Tok.reservedNames = []
  ,Tok.reservedOpNames = []
  ,Tok.caseSensitive=  True
}

data Expr
  = Tr
  | Fl
  | Zero
  | IsZero Expr
  | Succ Expr
  | Pred Expr
  | If Expr Expr Expr
  deriving (Eq,Show)


lexer :: Tok.TokenParser ()
lexer = Tok.makeTokenParser langDef

parens :: Parser a -> Parser a
parens = Tok.parens lexer  

reserved :: String -> Parser ()
reserved = Tok.reserved lexer

semiSep :: Parser a -> Parser [a]
semiSep = Tok.semiSep lexer

reservedOp :: String -> Parser ()
reservedOp = Tok.reservedOp lexer

prefixOp :: String -> (a -> a)   -> Ex.Operator String () Identity a
prefixOp s f = Ex.Prefix (reservedOp s >> return f)

table :: Ex.OperatorTable String () Identity Expr
table = [
  [
    prefixOp "succ" Succ
  ,prefixOp "pred" Pred
  ,prefixOp "iszero" IsZero
  ]
  ]  

expr :: Parser Expr
expr = Ex.buildExpressionParser table factor

factor :: Parser Expr
factor = true
  <|> false 
  <|> zero 
  <|> ifthen 
  <|> parens expr

true = reserved "true"   >>  return Tr
false = reserved "false"   >>  return Fl
zero = reserved "0"   >>  return Zero
  
ifthen :: Parser Expr
ifthen = do
  reserved "if"
  cond <- expr
  reserved "then"
  tr <- expr
  reserved "else"
  fl <- expr
  return (If cond tr fl)
  
contents :: Parser a -> Parser a
contents p = do
  Tok.whiteSpace lexer
  r <- p
  eof
  return r

toplevel :: Parser [Expr]
toplevel = semiSep expr

parseExpr :: String -> Either ParseError Expr
parseExpr s = parse (contents expr) "<stdin>" s

isNum Zero = True
isNum (Succ t) = isNum t
isNum _ = False

isVal :: Expr -> Bool
isVal Tr = True
isVal Fl = True
isVal t | isNum t = True
isVal _ = False

eval' x = case x of
  IsZero Zero -> Just Tr
  IsZero (Succ t) -> Just Fl
  IsZero t ->  IsZero <$> (eval' t)
  Succ t -> Succ <$> (eval' t)
  Pred Zero -> Just Zero
  Pred (Succ t) | isNum t -> Just t
  Pred t -> Pred <$> (eval' t)
  If Tr c _ -> Just c
  If Fl _ a -> Just a
  If t c a -> (\t' -> If t' c a) <$> eval' t
  _ -> Nothing

nf x = fromMaybe x $ nf <$> eval' x

eval :: Expr -> Maybe Expr
eval t = case nf t of
  nft | isVal nft -> Just nft
      | otherwise -> Nothing  

process :: String -> IO ()      
process line = do
  let res = parseExpr line
  case res of
    Left err -> print err
    Right ex -> case  eval ex  of
      Nothing -> putStrLn "Can't evaluate"
      Just result -> putStrLn $  show result

main :: IO ()      
main = runInputT defaultSettings loop
  where 
    loop = do
      minput <- getInputLine "Arith> "
      case minput of
        Nothing -> outputStrLn "Goodbye"
        Just input -> (liftIO $ process input) >> loop