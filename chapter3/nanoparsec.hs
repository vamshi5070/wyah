module NanoParsec where

import Data.Char
import Control.Monad
import Control.Applicative

newtype Parser a = Parser {parse :: String -> Maybe (a,String)}

runParser :: Parser a -> String -> a
runParser m s = case parse m s of
                  Just (res, []) -> res
                  Just (_,rs) -> error "Parser did not consume"
                  _ ->   error "Parser error."

item :: Parser Char
item = Parser $ \s ->
  case s of
    [] -> Nothing
    (c:cs) -> Just (c,cs)

bind :: Parser a -> (a -> Parser b) -> Parser b
bind p f = Parser $ \s -> case parse p s of
      Nothing -> Nothing
      Just (x,s') -> parse (f x) s'

instance Functor Parser where
  fmap f (Parser cs) = Parser (\s -> case (cs s) of
    Nothing -> Nothing
    Just (x,s' ) -> Just (f x,s'))

instance Applicative Parser where
  pure a = Parser (\s -> Just (a,s))
  (<*>) fp (Parser cs) = Parser (\s -> case (parse fp s) of
    Nothing -> Nothing
    Just (f,s') -> case cs s' of 
      Nothing -> Nothing
      Just (x,s'') -> Just (f x,s'')
      )

unit a = Parser (\s -> Just (a,s))

instance Monad Parser where 
--  return = unit
  (>>=) = bind

combine ps cs = Parser (\s -> case parse ps s of
  Nothing -> parse cs s
  Just (x,s') -> case parse cs s' of
    Nothing -> Nothing
    Just (x',s'') -> Just (x',s'')
  )

instance MonadPlus Parser where 
  mzero = failure
  mplus = combine

instance Alternative Parser where 
  empty = failure
  (<|>) = option

failure = Parser (\_ -> Nothing)  

option ps cs =  Parser (\s -> case parse ps s of
  Nothing -> parse cs s
  res -> res)
--  Just (x,s') -> Just (x,s'))

  

